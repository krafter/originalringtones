

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
//#import "MKStoreKit.h"
#import "MKStoreManager.h"
#import <MessageUI/MessageUI.h>

@interface ViewController : UIViewController <UIScrollViewDelegate, UIActionSheetDelegate, MFMailComposeViewControllerDelegate, AVAudioPlayerDelegate> {
    
    IBOutlet UIView *locateHelpView;
    IBOutlet UIButton *ninetynineButton0, *ninetynineButton1, *ninetynineButton2, *ninetynineButton3;
    IBOutlet UIButton *buyButton0, *buyButton1, *buyButton2, *buyButton3;
    IBOutlet UIButton *playButton0, *playButton1, *playButton2, *playButton3;
    
    IBOutlet UIScrollView *mainScrollView;
    
    AVAudioPlayer *theAudioPlayer;
    
    NSData *balcanData, *caribData, *guitarData, *jembeData;
    int currentPage;
    NSString *buyingProcessPurchaseID;
    BOOL justLoaded;
}

-(IBAction)buyEmailAction:(UIButton *)sender;

-(IBAction)playAction:(UIButton*)sender;

-(IBAction)balcanAction:(id)sender;
-(IBAction)guitarAction:(id)sender;
-(IBAction)carribeanAction:(id)sender;
-(IBAction)jembeAction:(id)sender;


@end

