

#import "InfoControllerViewController.h"

@interface InfoControllerViewController ()

@end

@implementation InfoControllerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@" InfoControllerViewController ");
}

- (void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(IBAction)emailAction:(id)sender {
    [self sendEmailToAuthor];
}


-(void)sendEmailToAuthor {
    
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setToRecipients:[NSArray arrayWithObject:@"vodahovski@gmail.com"]];
        [mailer setSubject:@""];
        [mailer setMessageBody:@"" isHTML:NO];
        [self presentViewController:mailer animated:YES completion:NULL];
    } else {
        NSLog(@" can't send!");
        UIAlertView *cantSendAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Can't Send Email", @"")
                                                                message:NSLocalizedString(@"Configure your email settings first", @"")
                                                               delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [cantSendAlert show];
    }
}

-(void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    if (result==MFMailComposeResultCancelled) { NSLog(@" canceled"); }
    if (result==MFMailComposeResultFailed) { NSLog(@" failed"); }
    if (result==MFMailComposeResultSaved) { NSLog(@" saved"); }
    if (result==MFMailComposeResultSent) { NSLog(@" sent"); }
    [controller dismissViewControllerAnimated:YES completion:NULL];
}




@end
