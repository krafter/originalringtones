//
//  InfoControllerViewController.h
//  OriginalRingtones
//
//  Created by Mac on 12/3/14.
//  Copyright (c) 2014 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface InfoControllerViewController : UIViewController <MFMailComposeViewControllerDelegate> {
    
}
-(IBAction)emailAction:(id)sender;


@end
