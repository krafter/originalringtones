#import "ViewController.h"

#import <sys/utsname.h>



#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4S_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)




#define INAPP_BALCAN_TONE_PRODUCT_ID        @"balkanicTuneID"
#define INAPP_CARIB_TONE_PRODUCT_ID         @"caribbeanTuneID"
#define INAPP_GUITAR_TONE_PRODUCT_ID        @"guitarSoloID"
#define INAPP_JEMBE_TONE_PRODUCT_ID         @"ethnicDrumsTuneID"



#import "RNEncryptor.h"
#import "RNDecryptor.h"


const int numPages = 4;

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITapGestureRecognizer *tapGest = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeLocateHelpView)];
    tapGest.numberOfTapsRequired = 1;
    tapGest.numberOfTouchesRequired = 1;
    [self.view addGestureRecognizer:tapGest];
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    justLoaded = YES;
}



-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (justLoaded==NO) { return; }
    justLoaded = NO;
    
    //layout stuff ===================================================================
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width*numPages,
                                            mainScrollView.frame.size.height);
    mainScrollView.delegate = self;
    
    //detect proper screen size and load proper interface
    NSString *balcanNibName = nil;
    NSString *guitarNibName = nil;
    NSString *carribeanNibName = nil;
    NSString *jembeNibName = nil;
    
    if (IS_IPHONE_4S_OR_LESS) {
        NSLog(@" IS_IPHONE_4S_OR_LESS ");
        balcanNibName = @"BalcanView_iPhone4";
        guitarNibName = @"GuitarView_iPhone4";
        carribeanNibName = @"Carribean_iPhone4";
        jembeNibName = @"Jembe_iPhone4";
    }
    
    if (IS_IPHONE_5) {
        NSLog(@" IS_IPHONE_5 ");
        balcanNibName = @"BalcanView_iPhone5";
        guitarNibName = @"GuitarView_iPhone5";
        carribeanNibName = @"Carribean_iPhone5";
        jembeNibName = @"Jembe_iPhone5";
    }
    
    if (IS_IPHONE_6) {
        NSLog(@" IS_IPHONE_6");
        balcanNibName = @"BalcanView_iPhone6";
        guitarNibName = @"GuitarView_iPhone6";
        carribeanNibName = @"Carribean_iPhone6";
        jembeNibName = @"Jembe_iPhone6";
    }
    
    if (IS_IPHONE_6P) {
        NSLog(@" IS_IPHONE_6P");
        balcanNibName = @"BalcanView_iPhone6plus";
        guitarNibName = @"GuitarView_iPhone6plus";
        carribeanNibName = @"Carribean_iPhone6plus";
        jembeNibName = @"Jembe_iPhone6plus";
    }
    
    
    int pageNumber = 0;
    
    pageNumber= 0;
    UIView *pageView = nil;
    NSArray *topLevelObjects=[[NSBundle mainBundle]loadNibNamed:balcanNibName owner:self options:nil];
    if ([topLevelObjects count]>0) { pageView = topLevelObjects[0]; }
    
    if (pageView!=nil) {
        CGRect rect = pageView.frame;
        rect.origin.x = mainScrollView.frame.size.width*pageNumber;
        rect.origin.y = 0;
        rect.size = mainScrollView.frame.size;
        pageView.frame= rect;
        [mainScrollView addSubview:pageView];
    }
    
    pageNumber= 1;
    topLevelObjects=[[NSBundle mainBundle]loadNibNamed:guitarNibName owner:self options:nil];
    if ([topLevelObjects count]>0) { pageView = topLevelObjects[0]; }
    
    if (pageView!=nil) {
        CGRect rect = pageView.frame;
        rect.origin.x = mainScrollView.frame.size.width*pageNumber;
        rect.origin.y = 0;
        rect.size = mainScrollView.frame.size;
        pageView.frame= rect;
        [mainScrollView addSubview:pageView];
    }
    
    pageNumber= 2;
    topLevelObjects=[[NSBundle mainBundle]loadNibNamed:carribeanNibName owner:self options:nil];
    if ([topLevelObjects count]>0) { pageView = topLevelObjects[0]; }
    
    if (pageView!=nil) {
        CGRect rect = pageView.frame;
        rect.origin.x = mainScrollView.frame.size.width*pageNumber;
        rect.origin.y = 0;
        rect.size = mainScrollView.frame.size;
        pageView.frame= rect;
        [mainScrollView addSubview:pageView];
    }
    
    pageNumber= 3;
    topLevelObjects=[[NSBundle mainBundle]loadNibNamed:jembeNibName owner:self options:nil];
    if ([topLevelObjects count]>0) { pageView = topLevelObjects[0]; }
    
    if (pageView!=nil) {
        CGRect rect = pageView.frame;
        rect.origin.x = mainScrollView.frame.size.width*pageNumber;
        rect.origin.y = 0;
        rect.size = mainScrollView.frame.size;
        pageView.frame= rect;
        [mainScrollView addSubview:pageView];
    }

    
    
    
    
    NSString *passString = @"ObamaMama";
    
    NSString *filePath = nil;
    NSData *fileData = nil;
    NSError *error = nil;
    
    filePath = [[NSBundle mainBundle] pathForResource:@"balcanEncrypted" ofType:@"m4r"];
    fileData = [NSData dataWithContentsOfFile:filePath];
    balcanData = [RNDecryptor decryptData:fileData withPassword:passString error:&error];
    if (error) { NSLog(@" error decrypting balcan audio data = %@ ", error); }
    
    filePath = [[NSBundle mainBundle] pathForResource:@"caribEncrypted" ofType:@"m4r"];
    fileData = [NSData dataWithContentsOfFile:filePath];
    caribData = [RNDecryptor decryptData:fileData withPassword:passString error:&error];
    if (error) { NSLog(@" error decrypting carib audio data = %@ ", error); }
    
    filePath = [[NSBundle mainBundle] pathForResource:@"guitarEncrypted" ofType:@"m4r"];
    fileData = [NSData dataWithContentsOfFile:filePath];
    guitarData = [RNDecryptor decryptData:fileData withPassword:passString error:&error];
    if (error) { NSLog(@" error decrypting guitar audio data = %@ ", error); }
    
    filePath = [[NSBundle mainBundle] pathForResource:@"jembeEncrypted" ofType:@"m4r"];
    fileData = [NSData dataWithContentsOfFile:filePath];
    jembeData = [RNDecryptor decryptData:fileData withPassword:passString error:&error];
    if (error) { NSLog(@" error decrypting jembe audio data = %@ ", error); }
    [self scrollViewDidScroll:mainScrollView];
    
    
    NSArray *locateViewTopLevelObjets=[[NSBundle mainBundle]loadNibNamed:@"LocateHelpView" owner:self options:nil];
    if ([locateViewTopLevelObjets count]>0) { locateHelpView = locateViewTopLevelObjets[0]; }
    if (locateHelpView!=nil) {
        CGRect rect = locateHelpView.frame;
        rect.size.width = mainScrollView.frame.size.width*0.75;
        locateHelpView.frame= rect;
    }
    locateHelpView.layer.cornerRadius = 12;
    locateHelpView.layer.masksToBounds = YES;
}


    
-(void) scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat WOffset = scrollView.contentOffset.x;
    int currPage = WOffset/scrollView.frame.size.width;
    if (currPage!=currentPage) {
        if (theAudioPlayer.isPlaying) { [theAudioPlayer stop]; }
        [self removeLocateHelpView];
        UIImage *playPas =[UIImage imageNamed:@"play_button_passive"];
        UIImage *playAct = [UIImage imageNamed:@"play_button_active"];
        [playButton0 setImage:playPas forState:UIControlStateNormal];
        [playButton1 setImage:playPas forState:UIControlStateNormal];
        [playButton2 setImage:playPas forState:UIControlStateNormal];;
        [playButton3 setImage:playPas forState:UIControlStateNormal];;
        
        [playButton0 setImage:playAct forState:UIControlStateHighlighted];
        [playButton1 setImage:playAct forState:UIControlStateHighlighted];
        [playButton2 setImage:playAct forState:UIControlStateHighlighted];
        [playButton3 setImage:playAct forState:UIControlStateHighlighted];
    }
    currentPage = currPage;
    
    NSString *inappPurchaseID = nil;
    if (currentPage==0) { inappPurchaseID = INAPP_BALCAN_TONE_PRODUCT_ID; }
    if (currentPage==1) { inappPurchaseID = INAPP_GUITAR_TONE_PRODUCT_ID; }
    if (currentPage==2) { inappPurchaseID = INAPP_CARIB_TONE_PRODUCT_ID;  }
    if (currentPage==3) { inappPurchaseID = INAPP_JEMBE_TONE_PRODUCT_ID;  }
    
    if([MKStoreManager isFeaturePurchased:inappPurchaseID]) {
        //TODO: UI++++++++++++++++++++++++++++++++
        if (currentPage==0) {
            [buyButton0 setTitle:NSLocalizedString(@"Email", @"") forState:UIControlStateNormal];
            [ninetynineButton0 setTitle:NSLocalizedString(@"Locate", @"") forState:UIControlStateNormal];
        }
        if (currentPage==1) {
            [buyButton1 setTitle:NSLocalizedString(@"Email", @"") forState:UIControlStateNormal];
            [ninetynineButton1 setTitle:NSLocalizedString(@"Locate", @"") forState:UIControlStateNormal];
        }
        if (currentPage==2) {
            [buyButton2 setTitle:NSLocalizedString(@"Email", @"") forState:UIControlStateNormal];
            [ninetynineButton2 setTitle:NSLocalizedString(@"Locate", @"") forState:UIControlStateNormal];
        }
        if (currentPage==3) {
            [buyButton3 setTitle:NSLocalizedString(@"Email", @"") forState:UIControlStateNormal];
            [ninetynineButton3 setTitle:NSLocalizedString(@"Locate", @"") forState:UIControlStateNormal];
        }
    } else {
        if (currentPage==0) {
            [buyButton0 setTitle:NSLocalizedString(@"BUY", @"") forState:UIControlStateNormal];
            [ninetynineButton0 setTitle:NSLocalizedString(@"0.99$", @"") forState:UIControlStateNormal];
        }
        if (currentPage==1) {
            [buyButton1 setTitle:NSLocalizedString(@"BUY", @"") forState:UIControlStateNormal];
            [ninetynineButton1 setTitle:NSLocalizedString(@"0.99$", @"") forState:UIControlStateNormal];
        }
        if (currentPage==2) {
            [buyButton2 setTitle:NSLocalizedString(@"BUY", @"") forState:UIControlStateNormal];
            [ninetynineButton2 setTitle:NSLocalizedString(@"0.99$", @"") forState:UIControlStateNormal];
        }
        if (currentPage==3) {
            [buyButton3 setTitle:NSLocalizedString(@"BUY", @"") forState:UIControlStateNormal];
            [ninetynineButton3 setTitle:NSLocalizedString(@"0.99$", @"") forState:UIControlStateNormal];
        }
    }
}











#pragma mark Buying, Email and Locate mechanism

-(IBAction)ninetynineLocateAction:(UIButton*)sender {
    NSString *inappPurchaseID = nil;
    if (sender.tag==0) { //balcan page
        inappPurchaseID = INAPP_BALCAN_TONE_PRODUCT_ID;
    }
    if (sender.tag==1) { //guitar page
        inappPurchaseID = INAPP_GUITAR_TONE_PRODUCT_ID;
    }
    if (sender.tag==2) { //carib page
        inappPurchaseID = INAPP_CARIB_TONE_PRODUCT_ID;
    }
    if (sender.tag==3) { //jembe page
        inappPurchaseID = INAPP_JEMBE_TONE_PRODUCT_ID;
    }
    BOOL alreadyBought = [MKStoreManager isFeaturePurchased:inappPurchaseID];
    if (alreadyBought) {
        [self locateAction];
    } else { //ask to buy or restore
        buyingProcessPurchaseID = inappPurchaseID;
        [self askToBuyOrRestore];
    }
}


-(IBAction)buyEmailAction:(UIButton *)sender {
    
    NSString *inappPurchaseID = nil;
    if (sender.tag==0) { //balcan page
        inappPurchaseID = INAPP_BALCAN_TONE_PRODUCT_ID;
    }
    if (sender.tag==1) { //guitar page
        inappPurchaseID = INAPP_GUITAR_TONE_PRODUCT_ID;
    }
    if (sender.tag==2) { //carib page
        inappPurchaseID = INAPP_CARIB_TONE_PRODUCT_ID;
    }
    if (sender.tag==3) { //jembe page
        inappPurchaseID = INAPP_JEMBE_TONE_PRODUCT_ID;
    }
    BOOL alreadyBought = [MKStoreManager isFeaturePurchased:inappPurchaseID];
    if (alreadyBought) {
        [self emailRingtoneWithNumber:(int)sender.tag];
    } else { //ask to buy or restore
        buyingProcessPurchaseID = inappPurchaseID;
        [self askToBuyOrRestore];
    }
}


-(void) askToBuyOrRestore {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Buy or Restore purchase?", @"")
                                                             delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Buy", @""), NSLocalizedString(@"Restore", @""), nil];
    CGRect viewFrame = self.view.frame;
    [actionSheet showFromRect:CGRectMake(viewFrame.size.width/2.0, viewFrame.size.height, 1, 1) inView:self.view animated:YES];
}



-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:NSLocalizedString(@"Buy", @"")]) {
        [self showActivityAnimation];
        [[MKStoreManager sharedManager] buyFeature:buyingProcessPurchaseID onComplete:^(NSString *purchasedFeature, NSData *purchasedReceipt, NSArray *availableDownloads) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Purchase complete", @"")
                                                            message:nil
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [self removeActivityAnimation];
        } onCancelled:^{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Purchase cancelled", @"")
                                                            message:NSLocalizedString(@"Try again if store was switched.", @"")
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            alert.tag = 1;
            [alert show];
            [self removeActivityAnimation];
        }];
        
    }
    if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:NSLocalizedString(@"Restore", @"")]) {
        [self showActivityAnimation];
        [[MKStoreManager sharedManager] restorePreviousTransactionsOnComplete:^{
            NSLog(@" purchases restored ");
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Purchases Restored", @"") message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            if([MKStoreManager isFeaturePurchased:INAPP_BALCAN_TONE_PRODUCT_ID]) {
                [self copyRingtoneToDocumentsFolder:0];
                [buyButton0 setTitle:NSLocalizedString(@"Email", @"") forState:UIControlStateNormal];
                [ninetynineButton0 setTitle:NSLocalizedString(@"Locate", @"") forState:UIControlStateNormal];
            }
            if([MKStoreManager isFeaturePurchased:INAPP_GUITAR_TONE_PRODUCT_ID]) {
                [self copyRingtoneToDocumentsFolder:1];
                [buyButton1 setTitle:NSLocalizedString(@"Email", @"") forState:UIControlStateNormal];
                [ninetynineButton1 setTitle:NSLocalizedString(@"Locate", @"") forState:UIControlStateNormal];
            }
            if([MKStoreManager isFeaturePurchased:INAPP_CARIB_TONE_PRODUCT_ID]) {
                [self copyRingtoneToDocumentsFolder:2];
                [buyButton2 setTitle:NSLocalizedString(@"Email", @"") forState:UIControlStateNormal];
                [ninetynineButton2 setTitle:NSLocalizedString(@"Locate", @"") forState:UIControlStateNormal];
            }
            if([MKStoreManager isFeaturePurchased:INAPP_JEMBE_TONE_PRODUCT_ID]) {
                [self copyRingtoneToDocumentsFolder:3];
                [buyButton3 setTitle:NSLocalizedString(@"Email", @"") forState:UIControlStateNormal];
                [ninetynineButton3 setTitle:NSLocalizedString(@"Locate", @"") forState:UIControlStateNormal];
            }
            //TODO: UI++++++++++++++++++++++++++++++++++++++

        } onError:^(NSError *error) {
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Can't Restore Purchases", @"") message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }];
    }
}




///number starts with 0
-(void) emailRingtoneWithNumber:(int) ringtoneNum {
    NSLog(@" email Ringtone with num = %d", ringtoneNum);
    [self sendEmailWithRingtone:ringtoneNum];
}

-(void) locateAction {
    NSLog(@" locateAction ");
    locateHelpView.center = CGPointMake(mainScrollView.frame.size.width/2.0,
                                        mainScrollView.frame.size.height/2.0);
    [self.view addSubview:locateHelpView];
}

-(void) removeLocateHelpView {
    if ([locateHelpView isDescendantOfView:self.view]) {
        [locateHelpView removeFromSuperview];
    }
}





-(IBAction)playAction:(UIButton*)sender {

    UIButton *buttonToModify = nil;
    NSData *theDataToPlay = nil;
    
    if (sender.tag==0) { //balcan page
        theDataToPlay = balcanData;
        buttonToModify = playButton0;
    }
    
    if (sender.tag==1) { //guitar page
        theDataToPlay = guitarData;
        buttonToModify = playButton1;
    }
    
    if (sender.tag==2) { //carib page
        theDataToPlay = caribData;
        buttonToModify = playButton2;
    }
    
    if (sender.tag==3) { //jembe page
        theDataToPlay = jembeData;
        buttonToModify = playButton3;
    }
    
    
    if (theAudioPlayer.isPlaying==YES) {
        [buttonToModify setImage:[UIImage imageNamed:@"play_button_passive"] forState:UIControlStateNormal];
        [buttonToModify setImage:[UIImage imageNamed:@"play_button_active"] forState:UIControlStateHighlighted];
        [theAudioPlayer stop];
        return;
    }
    
    [buttonToModify setImage:[UIImage imageNamed:@"pause_passive"] forState:UIControlStateNormal];
    [buttonToModify setImage:[UIImage imageNamed:@"pause_active"] forState:UIControlStateHighlighted];

    
    __autoreleasing NSError *plError = nil;
    theAudioPlayer = nil;
    theAudioPlayer = [[AVAudioPlayer alloc] initWithData:theDataToPlay
                                            fileTypeHint:AVFileTypeAppleM4A error:&plError];
    theAudioPlayer.delegate = self;
    theAudioPlayer.numberOfLoops = -1;
    if (plError) { NSLog(@" Player Error = %@", plError); }
    [theAudioPlayer play];
}


#pragma mark Switch page action

-(IBAction)balcanAction:(id)sender {
    CGFloat XOffset = mainScrollView.frame.size.width*0;
    [mainScrollView setContentOffset:CGPointMake(XOffset, 0) animated:YES];
}

-(IBAction)guitarAction:(id)sender {
    CGFloat XOffset = mainScrollView.frame.size.width*1;
    [mainScrollView setContentOffset:CGPointMake(XOffset, 0) animated:YES];
}

-(IBAction)carribeanAction:(id)sender {
    CGFloat XOffset = mainScrollView.frame.size.width*2;
    [mainScrollView setContentOffset:CGPointMake(XOffset, 0) animated:YES];
}

-(IBAction)jembeAction:(id)sender {
    CGFloat XOffset = mainScrollView.frame.size.width*3;
    [mainScrollView setContentOffset:CGPointMake(XOffset, 0) animated:YES];
}


-(IBAction)infoAction:(id)sender {
}


- (IBAction)unwind:(UIStoryboardSegue *)unwindSegue
{
    
}


#pragma mark Helpers

-(void) showActivityAnimation {
    
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 90, 90)];
    backView.backgroundColor = [UIColor blackColor];
    backView.layer.cornerRadius = 12;
    backView.layer.masksToBounds = YES;
    backView.tag = 313;
    backView.center = CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0);
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    indicator.center = CGPointMake(backView.frame.size.width/2.0, backView.frame.size.height/2.0);
    [indicator startAnimating];
    [backView addSubview:indicator];
    [self.view addSubview:backView];
}

-(void) removeActivityAnimation {
    UIView *toRemove = [self.view viewWithTag:313];
    if (toRemove!=nil) { [toRemove removeFromSuperview]; }
}

-(void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    UIImage *playPas =[UIImage imageNamed:@"play_button_passive"];
    UIImage *playAct = [UIImage imageNamed:@"play_button_active"];
    [playButton0 setImage:playPas forState:UIControlStateNormal];
    [playButton1 setImage:playPas forState:UIControlStateNormal];
    [playButton2 setImage:playPas forState:UIControlStateNormal];;
    [playButton3 setImage:playPas forState:UIControlStateNormal];;
    
    [playButton0 setImage:playAct forState:UIControlStateHighlighted];
    [playButton1 setImage:playAct forState:UIControlStateHighlighted];
    [playButton2 setImage:playAct forState:UIControlStateHighlighted];
    [playButton3 setImage:playAct forState:UIControlStateHighlighted];
}

-(void) deleteAllRingtoneFilesFromDocuments {
    NSString *docDir = [self documentsPath];
    NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:docDir error:nil];
    NSLog(@" dirCOnt = %@", dirContents);
    for (NSString *fileName in dirContents) {
        NSString *filePath = [docDir stringByAppendingPathComponent:fileName];
        if ([[filePath pathExtension] isEqualToString:@"m4r"]) {
            [self deleteFileAtPath:filePath];
        }
    }
}

-(void) copyRingtoneToDocumentsFolder:(int) ringtoneNum {
    
    NSData *ringtoneData = nil;
    NSString *fileName = nil;
    
    if (ringtoneNum==0) {
        ringtoneData = balcanData;
        fileName = @"balkanic.m4r";
    }
    
    if (ringtoneNum==1) {
        ringtoneData = guitarData;
        fileName = @"GuitarSolo.m4r";
    }
    
    if (ringtoneNum==2) {
        ringtoneData = caribData;
        fileName = @"Caribbean.m4r";
    }
    
    if (ringtoneNum==3) {
        ringtoneData = jembeData;
        fileName = @"EthnicDrums.m4r";
    }
    
    NSString *docDir = [self documentsPath];
    NSString *filePath = [docDir stringByAppendingPathComponent:fileName];
    [ringtoneData writeToFile:filePath atomically:YES];
}


-(void) showLocateMessageView {
    
}

-(void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    if (result==MFMailComposeResultCancelled) { NSLog(@" canceled"); }
    if (result==MFMailComposeResultFailed) { NSLog(@" failed"); }
    if (result==MFMailComposeResultSaved) { NSLog(@" saved"); }
    if (result==MFMailComposeResultSent) { NSLog(@" sent"); }
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

-(void)sendEmailWithRingtone:(int)ringtoneNum {
    
    NSData *ringtoneData = nil;
    NSString *fileName = nil;
    NSString *ringtoneTitle = nil;
    NSString *ringtoneDesc = nil;
    
    if (ringtoneNum==0) {
        ringtoneData = balcanData;
        fileName = @"balkanic.m4r";
        ringtoneTitle = @"Balkanic";
        ringtoneDesc = NSLocalizedString(@"This joyful melody comes from southeastern Europe, where the sun shines almost all year round and people do things leisurely.", @"");
    }
    
    if (ringtoneNum==1) {
        ringtoneData = guitarData;
        fileName = @"GuitarSolo.m4r";
        ringtoneTitle = @"Guitar Solo";
        ringtoneDesc = NSLocalizedString(@"You don't want to dance when hearing guitar solo. They say it's because these sounds are made for soul, not for hips.", @"");
    }
    
    if (ringtoneNum==2) {
        ringtoneData = caribData;
        fileName = @"Caribbean.m4r";
        ringtoneTitle = @"Caribbean";
        ringtoneDesc = NSLocalizedString(@"This tunes invites you to a place everybody should visit at least once in a lifetime.", @"");
    }
    
    if (ringtoneNum==3) {
        ringtoneData = jembeData;
        fileName = @"EthnicDrums.m4r";
        ringtoneTitle = @"Ethnic Drums";
        ringtoneDesc = NSLocalizedString(@"This time Africa, cradle of humankind calls you with its ethnic drums.", @"");
    }

    
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        
        [mailer addAttachmentData:ringtoneData mimeType:@"audio/mp4a-latm" fileName:fileName];
        [mailer setToRecipients:[NSArray array]];
        [mailer setSubject:ringtoneTitle];
        [mailer setMessageBody:ringtoneDesc isHTML:NO];
        
        [self presentViewController:mailer animated:YES completion:NULL];
    } else {
    
    NSLog(@" can't send!");
    UIAlertView *cantSendAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Can't Send Email", @"")
                                                            message:NSLocalizedString(@"Configure your email settings first", @"")
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [cantSendAlert show];
    }
}


-(void) deleteFileAtPath:(NSString*) filePath {
    if (filePath!=nil && [[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        NSError *deleteErr = nil;
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:&deleteErr];
        if (deleteErr) { NSLog (@"Can't delete file %@, ERROR - %@", filePath, deleteErr); }
    }
}


-(NSString *) documentsPath {
    NSString *root = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES) objectAtIndex:0];
    return root;
}

//-(NSString*) deviceModel {
//   
//    struct utsname systemInfo;
//    uname(&systemInfo);
//    
//    return [NSString stringWithCString:systemInfo.machine
//                              encoding:NSUTF8StringEncoding];
//
//}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end